document.write(`
<div class="tdates">
    <h3>Term Dates</h3>
    <ul>
        <li><strong>Autumn Term Dates:</strong></li>
        <li>Last swim: Thursday 12th December</li>
        <li><strong>Spring Term Dates:</strong></li>
        <li>Monday 6th January - Saturday 28th March</li>
        <li><i>(Half Term Sunday 16th February - Sunday 23rd February)</i></li>
    </ul>
</div>
`);
